# Алгоритм SAXPY

import time

# Начало подсчета времени выполнения
timeStart = time.perf_counter()


def saxpy(a, n):
    result = []
    # Заполнение массивовa x и y числами
    x = [i for i in range(n)]
    y = [i for i in range(n)]

    # Вычисление
    for i in range(0, n):
        result.append(a * x[i] + y[i])

    return result


# Вычислние 25000 раз
res = saxpy(3, 25000)
print(res)

# Конец подсчета времени выполнения
timeEnd = time.perf_counter() - timeStart
print(f"Время работы: {timeEnd}")

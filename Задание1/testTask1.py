# Вывод 

count = 42
file = open("file.txt", "r") # Открытие файла

# Сложение 
while True:
    line = file.readline()  # Получение строки
    if not line:
        break

    count += 1 / int(line)  # Вычисление

file.close()
print(f'Сумма ряда: {count}')


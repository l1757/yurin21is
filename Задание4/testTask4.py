# Алгоритм умножения матриц

import random


# Функция для умножения двух матриц
def calcMatrix(matxF, matxS):
    num = 0  # Число матрицы при вычислении
    array = []  # Строка матрицы
    matxT = []  # Конечная матрица

    rowsFirst = len(matxF)  # Строка первой матрицы
    colsFirst = len(matxF[0])  # Столбец первой матрицы
    colsSecond = len(matxS[0])  # Столбец второй матрицы

    # Цикл для умножения матриц
    for z in range(0, rowsFirst):
        for j in range(0, colsSecond):
            for i in range(0, colsFirst):
                num = num + matxF[z][i] * matxS[i][j]  # Вычисление числа матрицы
            array.append(num)  # Добавление числа в строку
            num = 0
        matxT.append(array)  # Добавление строки в матрицу
        array = []

    return matxT


# Функция для создание матриц заданной длины
def createMatrix(rows, cols):
    array = []  # Строка матрицы
    matx = []  # Конечная матрица

    # Цикл для заполнения массива
    for i in range(0, rows):
        for j in range(0, cols):
            array.append(random.randint(0, 10))  # Заполнение матрицы числами от 0 до 10

        matx.append(array)  # Добавление строки в матрицу
        array = []

    return matx


# Вычисление матриц с заданными границами
for i in range(10, 110, 10):
    print(calcMatrix(createMatrix(i, i), createMatrix(i, i)))  # Вывод результата
